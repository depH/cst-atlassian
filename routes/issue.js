const express = require('express');
const router = express.Router();
const request = require('request-promise');
const Promise = require('bluebird');

var AWS = require('aws-sdk');
var myCredentials = new AWS.Credentials("x", "x");

var sqs = new AWS.SQS({
  apiVersion: '2012-11-05',
  credentials: myCredentials,
  region: "none",
  endpoint: "http://localhost:9324"
});

const QUEUE = process.env.QUEUE_URL;
const JIRA_BASE_URL = process.env.JIRA_BASE_URL;

/**
 * API for pushing query to Queue
 * /rest/api/2/search?q=type=Bug
 */
router.get('/sum', function(req, res, next) {

  let query = req.query.query;
  let desc = req.query.name;
  var options = {
    uri: `${JIRA_BASE_URL}/rest/api/2/search`,
    qs: {
      q: query // Query string
    },
    headers: {
      'Content-Type': 'application/json'
    },
    json: true // Automatically parses the JSON string in the response
  };

  let result = {
    "name": desc,
    "totalPoints": 0
  };

  return request(options)
    .then(function (data) {

      result.totalPoints = countPoints(data);
      return pushToQueue(result)
    })
    .then(data => {
      if(!data)
      result["queueResponse"] = data;
      return res.send(result);
    })
    .catch(function (err) {
      let resp= {};
      if(err && err["name"] && err["name"] === 'RequestError') {
        resp["message"] = "JIRA_BASE_URL not accessible"
      }
      if(err && err["code"] && err["code"] === 'NetworkingError') {
        resp["message"] = "SQS not accessible"
      }

      resp["error"] = err;
      res.status(503);
      return res.send(resp);
    });
});

let countPoints = (data) => {
  let sum = 0;

  if(data && data.length) {
    data.forEach(item => {
      sum += item.fields.storyPoints;
    });
  }

  return sum;
};

let pushToQueue = (result) => {
  return new Promise((resolve, reject) => {
    var params = {
      MessageBody: JSON.stringify(result),
      QueueUrl: QUEUE,
      DelaySeconds: 0,
    };
    sqs.sendMessage(params, function(err, data) {
      if (err) reject(err); // an error occurred

      resolve(data);
    });
  })
}

module.exports = router;
